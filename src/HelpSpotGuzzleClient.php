<?php namespace HelpSpot;

use Hampel\Json\Json;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Hampel\Json\JsonException;
use GuzzleHttp\Client as GuzzleClient;
use HelpSpot\Exception\RuntimeException;
use GuzzleHttp\Exception\ParseException;
use GuzzleHttp\Exception\RequestException;
use HelpSpot\Exception\HelpSpotParseException;
use HelpSpot\Exception\HelpSpotRequestException;
use GuzzleHttp\ClientInterface as GuzzleClientInterface;

/**
 * The main service interface using Guzzle
 */
class HelpSpotGuzzleClient implements HelpSpotClient
{
	/** @var string api username for auth */
	protected $username;

	/** @var string api password for auth */
	protected $password;

	/** @var HelpSpotGuzzleClient our Guzzle HTTP Client object */
	protected $client;

	/** @var Request Psr7 Request object representing the last request made */
	protected $last_request;

	/** @var Response Guzzle Response object representing the last response from Guzzle call to HelpSpot API */
	protected $last_response;

	/** @var  string a string description of the last action taken */
	protected $last_action;

	/**
	 * Constructor
	 *
	 * @param GuzzleClientInterface $client	Guzzle HTTP client
	 */
	public function __construct(GuzzleClientInterface $client, $username, $password)
	{
		$this->client = $client;
		$this->username = $username;
		$this->password = $password;
	}

	/**
	 * Make - construct a service object
	 *
	 * @param string $username
	 * @param string $password
	 * @param string $base_uri
	 *
	 * @return HelpSpotGuzzleClient a fully hydrated HelpSpot Service, ready to run
	 */
	public static function make($username, $password, $base_uri)
	{
		return new self(new GuzzleClient(self::getConfig($base_uri)), $username, $password);
	}

	public static function getConfig($base_uri)
	{
		return ['base_uri' => $base_uri];
	}

	public function getClient()
	{
		return $this->client;
	}

	public function get($action)
	{
		$this->last_action = "GET {$action}";

		return $this->send($action, 'GET');
	}

	public function post($action, array $data = [])
	{
		$this->last_action = "POST {$action}";

		return $this->send($action, 'POST', $data);
	}

	public function put($action, array $data = [])
	{
		$this->last_action = "PUT {$action}";

		return $this->send($action, 'PUT', $data);
	}

	public function delete($action)
	{
		$this->last_action = "DELETE {$action}";

		return $this->send($action, 'DELETE');
	}

	public function send($action, $method = 'GET', $data = [], array $options = [])
	{
		if (empty($this->username))
		{
			throw new RuntimeException("Username not yet set");
		}

		if (empty($this->password))
		{
			throw new RuntimeException("Password not yet set");
		}

		if (!array_key_exists('auth', $options))
		{
			$options['auth'] = [$this->username, $this->password];
		}

		$json = null;
		if (!empty($data))
		{
			try
			{
				$json = Json::encode($data);
			}
			catch (JsonException $e)
			{
				throw new RuntimeException("Could not encode JSON payload: " . $e->getMessage(), $e->getCode(), $e);
			}
		}

		$headers = [];

		$request = new Request($method, $action, $headers, $json);

		$this->last_request = $request;

		try
		{
			$response = $this->client->send($request, $options);
		}
		catch (RequestException $e)
		{
			throw new HelpSpotRequestException($e->getMessage(), $e->getCode(), $e);
		}

		$this->last_response = $response;

		$body = $response->getBody();

		if ($body->getSize() > 0)
		{
			try
			{
				return Json::decode($body->getContents(), Json::DECODE_ASSOC);
			}
			catch (ParseException $e)
			{
				throw new HelpSpotParseException(
					"HelpSpot " . $e->getMessage() . " - last command [{$this->last_action}]",
					$e->getCode(),
					$e
				);
			}
		}
	}

	/**
	 * Return the request object from the last API call made
	 *
	 * @return Request Psr7 Request object
	 */
	public function getLastRequest()
	{
		return $this->last_request;
	}

	/**
	 * Return the response object from the last API call made
	 *
	 * @return Response Guzzle Reponse object
	 */
	public function getLastResponse()
	{
		return $this->last_response;
	}

	/**
	 * Return the status code from the last API call made
	 *
	 * @return number status code
	 */
	public function getLastStatusCode()
	{
		$last_response = $this->getLastResponse();
		if (! is_null($last_response))
		{
			return $last_response->getStatusCode();
		}
	}

	public function getLastQuery()
	{
		$last_request = $this->getLastRequest();
		if (!is_null($last_request))
		{
//			return strval(\GuzzleHttp\Psr7\Uri::resolve(\GuzzleHttp\Psr7\uri_for($this->client->getConfig('base_uri')), $last_request->getUri()));
			return strval($last_request->getUri());
		}
	}

	public function getLastAction()
	{
		return $this->last_action;
	}
}
