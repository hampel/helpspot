<?php namespace HelpSpot;

use HelpSpot\Types\EmailAddress;

class HelpSpot
{
	/** @var HelpSpotClient our client implementation */
	protected $client;

	function __construct(HelpSpotClient $client)
	{
		$this->client = $client;
	}

	/**
	 * Fetch list of mailboxes
	 *
	 * @param $activeOnly	show active only (1 = active, 0 = both active and inactive)
	 *
	 * @return array
	 */
	public function getMailboxes($activeOnly = true)
	{
		$method = "private.request.getMailboxes";
		$output = "json";
		$fActiveOnly = $activeOnly ? "1" : "0";

		$querystring = http_build_query(compact('method', 'output', 'fActiveOnly'));

		return $this->client->get("?{$querystring}");
	}

	public function getMailboxIds($activeOnly = true)
	{
		$mailboxes = $this->getMailboxes($activeOnly);

		$mailboxIds = [];
		foreach ($mailboxes['mailbox'] as $mailbox)
		{
			$mailboxIds[$mailbox['xMailbox']] = EmailAddress::createEmailAddress($mailbox['sReplyEmail'], $mailbox['sReplyName'])->getFull();
		}

		return $mailboxIds;
	}

	public function getMailbox($id)
	{
		$mailboxes = $this->getMailboxIds(false);
		if (array_key_exists($id, $mailboxes)) return $mailboxes[$id];
	}
}
